import React from "react";

export interface TableRow {
  [key: string]: any;
}

interface TableProps {
  tableData: TableRow[];
  tableTitle: string;
  tableSubtitle?: string;
  fullWidth?: boolean;
  fullHeight?: boolean;
}

const StatTable = ({ tableData, tableTitle, tableSubtitle, fullWidth, fullHeight }: TableProps) => {
  const tableFields = Object.keys(tableData[0]);
  return (
    <div className={!fullWidth ? 'col-md-6 col-lg-6' : 'col-md-6 col-lg-12'}>
      <div className="row" style={{ margin: "5px", ...fullHeight && { height: "100%" } }}>
        <div className="card" id={tableTitle}>
          <div className="card-header card-header-primary">
            <h4 className="card-title">{tableTitle}</h4>
            <p className="card-category">{tableSubtitle}</p>
          </div>
          <div className="card-body table-responsive">
            <table className="table table-hover">
              <thead>
                {tableFields.map((field, i) => (
                  <th key={i}>
                    {field.charAt(0).toUpperCase() + field.slice(1)}
                  </th>
                ))}
              </thead>
              <tbody>
                {!!tableData.length &&
                  tableData.map((row: TableRow, i: number) => (
                    <tr key={i}>
                      {Object.values(row).map((item: any, j: number) => (
                        <td key={j}>{item}</td>
                      ))}
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StatTable;
