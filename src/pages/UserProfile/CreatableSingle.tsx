import React, { useEffect, useState } from "react";
import CreatableSelect from "react-select/creatable";
import { I_InstitutionListItem } from "./profileTypes";
import http from "../../config/http";

const customStyles = {
  option: (provided: any, state: { isSelected: any }) => ({
    ...provided,
    borderBottom: "1px dotted pink",
    padding: 10,
    color: "black",
  }),
  control: (base: any, state: any) => ({
    ...base,
    background: "transparent",
    // match with the menu
    borderRadius: state.isFocused ? "3px 3px 0 0" : 3,
    // Removes weird border around container
    boxShadow: state.isFocused ? null : null,
  }),
  singleValue: (provided: any, state: any) => ({
    ...provided,
    color: "white",
  }),
  input: (provided: any, state: any) => ({
    ...provided,
    color: "white",
  }),
};

interface IOption {
  label: string;
  value: number;
}
interface IProps {
  category: string;
  disabled: boolean;
  handleInstitutionChange: any;
  // setInstitutionId: any;
  // instName: string | null;
  instId: number | null;
}

const mapToFormat = (x: I_InstitutionListItem): IOption => ({
  label: x.name,
  value: x.id,
});

const defaultList: IOption[] = [
  {
    value: 0,
    label: "Select...",
  },
];
const NaList: IOption[] = [
  {
    value: 0,
    label: "N/A",
  },
];

const CreatableSingle = React.memo(
  ({
    disabled,
    category,
    handleInstitutionChange,
    instId,
    // setInstitutionId,
  }: IProps) => {
    const [options, setOptions] = useState<IOption[]>([
      {
        label: "",
        value: 0,
      },
    ]);
    const [selected, setSelected] = useState<IOption>(options[0]);

    const fetchAndSetInstList = (category: string) => {
      http
        .get(`/Institution/${category}/list`)
        .then((iList: I_InstitutionListItem[]) => {
          if (iList) {
            setOptions(iList.map((x) => mapToFormat(x)));
            const match_inst = iList.find(x => x.id === instId);
            // console.log('iList: ', iList);
            // console.log('instId: ', instId);
            // console.log('match_inst: ', match_inst);
            if (match_inst) {
              // setInstitutionId(match_inst.id); 
              handleInstitutionChange(match_inst.id, match_inst.name);
              setSelected({ label: match_inst.name || "", value: instId || 0 });
            } else {
              handleInstitutionChange(0, "");
              setSelected(defaultList[0]);
            }
          } else {
            handleInstitutionChange(0, "");
            setSelected(defaultList[0]);
            setOptions(defaultList);
          }
        });
    };

    useEffect(() => {
      if (category) {
        if (category !== "other") fetchAndSetInstList(category);
        else {
          handleInstitutionChange(null, null);
          setSelected(NaList[0]);
          setOptions(NaList);
        }
      } else {
        handleInstitutionChange(0, "");
        setSelected(defaultList[0]);
      }
    }, [category]);

    const handleChange = (newValue: any, actionMeta?: any) => {
      setSelected((inst) => ({ ...inst, ...newValue }));
      handleInstitutionChange(newValue.value, newValue.label);
    };
    const handleInputChange = (inputValue: any, actionMeta: any) => {
      // console.log('inputValue: ', inputValue);
    };
    const handleInstitutionCreate = (name: string) => {
      console.log("Instition to be created: ", name);
      handleChange({ value: 0, label: name });
      // fetchAndSetInstList(category);
    };
    return (
      <CreatableSelect
        styles={customStyles}
        options={options}
        value={selected}
        onChange={handleChange}
        onInputChange={handleInputChange}
        isDisabled={disabled}
        onCreateOption={handleInstitutionCreate}
        required
      />
    );
  }
);

export default CreatableSingle;
