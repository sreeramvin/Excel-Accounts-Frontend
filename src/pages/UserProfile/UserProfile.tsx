import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import "./UserProfile.scss";
import { IUpdateProfile } from "./profileTypes";
import { saveProfile, setProfile, saveReferralCode } from "../../store/Actions/userprofileActions";

import ViewProfile from "./ViewProfile";
import CreatableSingle from "./CreatableSingle";
import { selectUserprofile } from "../../store/Selectors/userprofileSelectors";
import http from "../../config/http";
import { selectAmbassadorId } from "../../store/Selectors/ambassadorSelectors";

const UserProfile = () => {
  const profile = useSelector(selectUserprofile);
  const ambassadorId: number = useSelector(selectAmbassadorId);

  const dispatch = useDispatch();

  const [updateProfile, setUpdateProfile] = useState<IUpdateProfile>({
    name: "",
    institutionId: 0,
    institutionName: "",
    gender: "",
    mobileNumber: "",
    category: "",
  });
  const [errorMsg, setErrorMsg] = useState<string>("");
  const [referralCode, setReferralCode] = useState<string>("");

  useEffect(() => {
    http.get('/Profile/view').then(res => {
      const { institutionName } = res;
      setProfile(dispatch, { institutionName });
      setUpdateProfile(updateProfile => ({ ...updateProfile, institutionName }));
    })
  }, []);

  useEffect(() => {
    setUpdateProfile((u: any) => ({
      ...u,
      name: profile.name,
      institutionId: profile.institutionId,
      institutionName: profile.institutionName,
      gender: profile.gender,
      mobileNumber: profile.mobileNumber,
      category: profile.category,
    }));
  }, [
    profile.name,
    profile.institutionId,
    profile.institutionName,
    profile.gender,
    profile.mobileNumber,
    profile.category,
  ]);

  useEffect(() => {
    setReferralCode(profile.referrerAmbassadorId);
  }, [profile.referrerAmbassadorId]);

  const [editable, setEditable] = useState<boolean>(false);
  var opts = { readOnly: !editable, disabled: !editable };

  const handleUpdateProfile = (event: {
    target: { name: any; value: any };
  }) => {
    const { name, value } = event.target;
    setUpdateProfile({ ...updateProfile, [name]: value });
  };
  const setInstitution = (id: number, name: string) => {
    // console.log('id: ', id);
    // console.log('name: ', name);
    setUpdateProfile({
      ...updateProfile,
      institutionId: id,
      institutionName: name,
    });
  };
  // const setInstitutionId = (id: number) => {
  //   setUpdateProfile({ ...updateProfile, institutionId: id });
  // };

  const handleEdit = (event: { preventDefault: () => void }) => {
    setEditable(true);
    opts = { readOnly: false, disabled: false };
    event.preventDefault();
  };

  const submitValue = (event: { preventDefault: () => void }) => {
    event.preventDefault();
    const {
      name,
      gender,
      category,
      mobileNumber,
      institutionName,
      institutionId
    } = updateProfile;

    if (name !== profile.name || gender !== profile.gender || category !== profile.category || mobileNumber !== profile.mobileNumber || institutionName !== profile.institutionName || institutionId !== profile.institutionId) {
      if ((name && name.trim()) && gender && category && (mobileNumber && mobileNumber.trim())) {
        if (category !== "other" && !institutionName) {
          setErrorMsg("Pls fill out the required fields!");
          return;
        } else {
          setErrorMsg("");
          saveProfile(dispatch, updateProfile);
        }
      } else {
        setErrorMsg("Pls fill out the required fields!");
        return;
      }
    }

    if (referralCode && !profile.referrerAmbassadorId) {
      const refId = Number(referralCode);
      if (refId === ambassadorId) {
        setErrorMsg("You can't refer yourself");
        return;
      }
      else {
        setErrorMsg("");
        saveReferralCode(refId);
      }
    }

    setEditable(false);
    opts = { readOnly: true, disabled: true };
  };

  return (
    <>
      <div className="content">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-8">
              <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">Edit Profile</h4>
                  <p className="card-category">Complete your profile</p>
                </div>
                <div className="card-body">
                  <form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">Name *</label>
                          <input
                            type="text"
                            className="form-control"
                            name="name"
                            value={updateProfile.name}
                            onChange={handleUpdateProfile}
                            required
                            {...opts}
                            style={{ backgroundColor: "transparent" }}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">Gender *</label>
                          <select
                            className="form-control"
                            name="gender"
                            value={updateProfile.gender || ''}
                            onChange={handleUpdateProfile}
                            {...opts}
                            style={{ backgroundColor: "transparent" }}
                            required
                          >
                            <option
                              value=""
                              style={{ color: "gray" }}
                              disabled
                            // selected
                            >
                              Select
                            </option>
                            <option value="Male" className="text-dark">
                              Male
                            </option>
                            <option value="Female" className="text-dark">
                              Female
                            </option>
                            <option value="Other" className="text-dark">
                              Other
                            </option>
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">
                            Category *
                          </label>
                          <select
                            className="form-control"
                            name="category"
                            value={updateProfile.category || ""}
                            onChange={handleUpdateProfile}
                            {...opts}
                            style={{ backgroundColor: "transparent" }}
                            required
                          >
                            <option
                              value=""
                              style={{ color: "gray" }}
                              disabled
                            // selected
                            >
                              Select
                            </option>
                            <option value="college" className="text-dark">
                              College
                            </option>
                            <option value="school" className="text-dark">
                              School
                            </option>
                            <option value="other" className="text-dark">
                              Other
                            </option>
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">
                            Institution Name *
                          </label>
                          <CreatableSingle
                            disabled={
                              !editable || updateProfile.category === "other"
                            }
                            category={updateProfile.category}
                            instId={updateProfile.institutionId}
                            handleInstitutionChange={setInstitution}
                          // setInstitutionId={setInstitutionId}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">
                            Mobile Number *
                          </label>
                          <input
                            type="number"
                            className="form-control"
                            name="mobileNumber"
                            value={updateProfile.mobileNumber}
                            onChange={handleUpdateProfile}
                            maxLength={10}
                            {...opts}
                            style={{ backgroundColor: "transparent" }}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">
                            Referral Code
                          </label>
                          <input
                            type="number"
                            className="form-control"
                            name="referralCode"
                            onChange={(e) => setReferralCode(e.target.value)}
                            value={referralCode || ''}
                            maxLength={10}
                            disabled={!!profile.referrerAmbassadorId}
                            readOnly={!!profile.referrerAmbassadorId}
                            style={{ backgroundColor: "transparent" }}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="text-danger">{errorMsg}</div>
                    <button
                      type="submit"
                      onClick={handleEdit}
                      className="btn btn-primary pull-right"
                      disabled={editable}
                    >
                      Edit Profile
                    </button>
                    <button
                      type="submit"
                      onClick={submitValue}
                      className="btn btn-primary pull-right"
                      disabled={!editable}
                    >
                      Update Profile
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <ViewProfile viewProfile={profile} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UserProfile;
