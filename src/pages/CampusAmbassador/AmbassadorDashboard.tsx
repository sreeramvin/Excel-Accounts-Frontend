import React, { useState, useEffect } from "react";
import StatCard from "../../components/AmbassadorDashboard/StatCard";
import StatTable, {
  TableRow,
} from "../../components/AmbassadorDashboard/StatTable";
import Referral from "../../components/AmbassadorDashboard/Referral";

import http from "../../config/http";

import "./AmbassadorDashboard.scss";

const defaultJoinedData: TableRow[] = [
  {
    users: "No referred users",
  },
];

const defaultRankData: TableRow[] = [
  {
    users: "Loading",
  },
];

interface IAmbassadorDashboardProps {
  ambassadorId: number;
}

const AmbassadorDashboard = ({ ambassadorId }: IAmbassadorDashboardProps) => {
  const [joinedData, setJoinedData] = useState<TableRow[]>(defaultJoinedData);
  const [rankData, setRankData] = useState<TableRow[]>(defaultRankData);
  const [currentAmbassadorPoints, setCurrentAmbassadorPoints] = useState(0);

  const sortAndRank = (rankData: any) => {
    for (let i = 0; i < rankData.length; i++) {
      rankData[i]["points"] =
        rankData[i].freeMembership * 5 + rankData[i].paidMembership * 20;

      if (rankData[i]["ambassadorId"] === ambassadorId) {
        setCurrentAmbassadorPoints(rankData[i]["points"]);
      }
    }

    rankData.sort((a: any, b: any) => {
      if (a.points === b.points) {
        return b.paidMembership > a.paidMembership
          ? 1
          : b.paidMembership < a.paidMembership
            ? -1
            : 0;
      }
      return b.points - a.points;
    });

    // rankData[0]["rank"] = 1;

    let newRankData: Array<{ rank: number }> = [];

    newRankData.push({ rank: 1 });
    newRankData[0] = { ...newRankData[0], ...rankData[0] };

    for (let i = 1; i < rankData.length; i++) {
      if (rankData[i]["points"] === rankData[i - 1]["points"]) {
        newRankData.push({ rank: newRankData[i - 1]["rank"] });
      } else {
        newRankData.push({ rank: i + 1 });
      }
      newRankData[i] = { ...newRankData[i], ...rankData[i] };
    }

    return newRankData;
  };

  useEffect(() => {
    http.get("/Ambassador/userlist").then((response) => {
      if (response.length) {
        let res = response.map((x: { email: string; name: string; isPaid: boolean }, i: number) => {
          const { isPaid, ...rest } = x;
          return {
            No: i + 1,
            ...rest,
            membership: isPaid ? 'Paid' : 'Free'
          }
        })
        setJoinedData(res);
      }
    })
  }, [])

  useEffect(() => {
    http.get("/Ambassador/list").then((response) => {
      setRankData(sortAndRank(response));
    });
  }, []);

  return (
    <>
      <div className='row'>
        <div className='col-md-4 col-lg-6'>
          <StatCard
            title={joinedData === defaultJoinedData ? 0 : joinedData.length}
            desc="Joined"
            icon="done_all"
            icon_bg="success"
            tableId="Joined"
          />
        </div>
        <div className='col-md-6 col-lg-6'>
          <StatCard
            title={currentAmbassadorPoints}
            desc="Points Earned"
            icon="attach_money"
            icon_bg="warning"
            tableId="Rank"
          />
        </div>
      </div>
      <div className="row row-2">
        <Referral />
        <StatTable tableData={joinedData} tableTitle="Joined" fullHeight />
      </div>
      <div className="row">
        <StatTable tableData={rankData} tableTitle="Rank" fullWidth />
      </div>
    </>
  );
};

export default AmbassadorDashboard;