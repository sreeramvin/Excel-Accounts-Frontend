import React, { lazy, Suspense } from "react";
import { useSelector } from "react-redux";
import DottedLineLoader from "../../components/common/Loaders/DottedLineLoader";

import { selectAmbassadorId, selectAmbassadorFetching } from "../../store/Selectors/ambassadorSelectors";

const JoinProgram = lazy(() => import("./JoinProgram"));
const AmbassadorDashboard = lazy(() => import("./AmbassadorDashboard"));

const CampusAmbassadorPage = () => {
  const ambassadorId = useSelector(selectAmbassadorId);
  const ambassadorLoading = useSelector(selectAmbassadorFetching);

  console.log({ ambassadorId })

  return (
    <Suspense fallback={<DottedLineLoader />}>
      {ambassadorId ? <AmbassadorDashboard ambassadorId={ambassadorId} /> : ambassadorLoading ? <DottedLineLoader className='fullCenter' /> : <JoinProgram />}
    </Suspense>
  );
};

export default CampusAmbassadorPage;
