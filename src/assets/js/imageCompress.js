import {compress} from 'image-conversion';

// const filetoDataURL = function (file) {
//     return new Promise((resolve) => {
//         const reader = new FileReader();
//         reader.onloadend = e => resolve(e.target.result);
//         reader.readAsDataURL(file);
//     });
// };
const dataURLToImage = (dataURL) => {
    return new Promise((resolve, reject) => {
        const img = new Image();
        img.onload = () => resolve(img);
        img.onerror = () => reject(new Error('dataURLtoImage(): dataURL is illegal'));
        img.src = dataURL;
    });
}

const maxHeight = 600, maxWidth = 800;
export const compressImages = (images) => {
    return images.map(async (imgObj) => {
        const dataURL = URL.createObjectURL(imgObj);
        const image = await dataURLToImage(dataURL);
        const actualHeight = image.height;
        const actualWidth = image.width;
        const imageRatio = actualWidth / actualHeight;

        let newWidth, newHeight;
        const maxRatio = maxWidth / maxHeight;
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imageRatio < maxRatio) { // If height is greater
                newWidth = (maxHeight / actualHeight) * actualWidth;
                newHeight = maxHeight;
            }
            else if (imageRatio > maxRatio) { // If width is greater
                newHeight = (maxWidth / actualWidth) * actualHeight;
                newWidth = maxWidth;
            }
            else {
                newHeight = maxHeight;
                newWidth = maxWidth;
            }
        }
        let compressedBlob = await compress(imgObj, {
            quality: 0.7,
            type: imgObj.type,
            width: newWidth,
            height: newHeight,
        });
        const file = new File([compressedBlob], imgObj.name, {lastModified: new Date()});
        return file;
    });
}