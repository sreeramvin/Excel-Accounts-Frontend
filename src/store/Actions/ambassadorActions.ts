import { SET_AMBASSADOR, SET_AMBASSADOR_FETCHING } from "../constants";
import http from "../../config/http";

export const setAmbassador = (dispatch: any, response: any) => {
  const { ambassadorId = 0, freeMembership = 0, paidMembership = 0 } =
    response || {};

  dispatch({
    type: SET_AMBASSADOR,
    payload: { ambassadorId, freeMembership, paidMembership },
  });
};

export const signUpForAmbassador = (dispatch: any) => {
  http.get("/Ambassador/signup").then((response) => {
    if (response.response === "Success") {
      http.get("/Ambassador").then((response) => {
        // console.log(response);
        setAmbassador(dispatch, response);
      });
    }
  });
};

export const setAmbassadorFetching = (dispatch: any, isFetching: boolean) => {
  dispatch({
    type: SET_AMBASSADOR_FETCHING,
    isFetching,
  });
};
