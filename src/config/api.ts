const hostname = "staging.accounts.excelmec.org";

export const WSRoot = `ws://${hostname}`;

const gateway = "staging.apis.excelmec.org";

export const endpoints = {
  ApiRoot: `https://${hostname}/api`,
  EventsApiRoot: `https://${gateway}/events/api`,
};
