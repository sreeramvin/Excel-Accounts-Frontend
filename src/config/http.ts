import { endpoints } from "./api";

interface postData {
  [key: string]: any;
}

const post = async (
  url: string,
  data: postData,
  endpoint: "ApiRoot" | "EventsApiRoot" = "ApiRoot"
) => {
  const headers: HeadersInit = new Headers();
  headers.append("Accept", "application/json");
  headers.append("Content-Type", "application/json");

  if (localStorage.getItem("jwt_token")) {
    headers.append(
      "Authorization",
      "Bearer " + localStorage.getItem("jwt_token")
    );
  }
  return fetch(endpoints[endpoint] + url, {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data),
  })
    .then((res: any) => res.json())
    .catch((err: Error) => err);
};

const get = async (
  url: string,
  endpoint: "ApiRoot" | "EventsApiRoot" = "ApiRoot"
) => {
  const headers: HeadersInit = new Headers();
  headers.append("Accept", "application/json");
  headers.append("Content-Type", "application/json");

  if (localStorage.getItem("jwt_token")) {
    headers.append(
      "Authorization",
      "Bearer " + localStorage.getItem("jwt_token")
    );
  }
  return fetch(endpoints[endpoint] + url, {
    method: "GET",
    headers: headers,
  })
    .then((res: any) => res.json())
    .catch((err: Error) => console.log(err));
};

const uploadImage = async (url: string, data: any) => {
  const headers: HeadersInit = new Headers();
  // headers.append('Accept', 'multipart/form-data');
  // headers.append('Content-Type', 'multipart/form-data');

  if (localStorage.getItem("jwt_token")) {
    headers.append(
      "Authorization",
      "Bearer " + localStorage.getItem("jwt_token")
    );
  }
  return fetch(endpoints["ApiRoot"] + url, {
    method: "POST",
    headers: headers,
    body: data,
  });
};

export default { post, get, uploadImage };
